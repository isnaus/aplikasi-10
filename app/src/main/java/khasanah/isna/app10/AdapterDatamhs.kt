package khasanah.isna.app10

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import android.widget.TextView
import com.squareup.picasso.Picasso

class AdapterDatamhs(val datamhs : List<HashMap<String,String>>) :
    RecyclerView.Adapter<AdapterDatamhs.HolderDataMhs>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterDatamhs.HolderDataMhs {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_mhs,p0, false)
        return HolderDataMhs(v)
    }

    override fun getItemCount(): Int {
        return datamhs.size
    }

    override fun onBindViewHolder(p0: HolderDataMhs, p1: Int) {
        val data = datamhs.get(p1)
        p0.txNim.setText(data.get("nim"))
        p0.txNama.setText(data.get("nama"))
        p0.txProdi.setText(data.get("prodi"))
        if (!data.get("url").equals(""))
        Picasso.get().load(data.get("url")).into(p0.photo);
    }

    class HolderDataMhs(v : View) : RecyclerView.ViewHolder(v){
        val txNim = v.findViewById<TextView>(R.id.txNim)
        val txNama = v.findViewById<TextView>(R.id.txNama)
        val txProdi = v.findViewById<TextView>(R.id.txProdi)
        val photo = v.findViewById<ImageView>(R.id.imageView)
    }

}